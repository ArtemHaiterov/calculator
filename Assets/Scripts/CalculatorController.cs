﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class CalculatorController : MonoBehaviour
{
    [SerializeField] 
    private Text _numberText;
    [SerializeField] 
    private Text _operatorText;
    [SerializeField]
    private Text _PerformedOperationsText;

    private double _currentValue;
    private double _storedValue;
    private double _result;

    private bool _divideError;
    private bool _specialAction;
    private bool _hasNumber;
    private bool _canChangeOperator;

    private string _memoryValue;
    private string _storedOperator;
    private string _firstPartOfPerformedOperation;
    private string _secondPartOfPerformedOperation;

    private const string CCaption = "c";
    private const string MSCaption = "MS";
    private const string MRCaption = "MR";
    private const string MCCaption = "MC";
    private const string MPlusCaption = "M+";
    private const string MMinusCaption = "M-";
    private const string BackCaption = "←";
    private const string CommaCaption = ",";
    private const string EmptyCaption = "";
    private const string PlusMinusCaption = "±";
    private const string PercentCaption = "%";
    private const string PlusCaption = "+";
    private const string MinusCaption = "-";
    private const string MultiplyCaption = "x";
    private const string DivideCaption = "÷";
    private const string EqualsCaption = "=";
    private const string ZeroCaption = "0";
    private const string SpaceCaption = " ";
    private const string RootCaption = "²√x";
    private const string LogarithmCaption = "log";
    private const string SecondDegreeCaption = "х²";
    private const string OneDivideXCaption = "1/x";
    private const string DivideErrorText = "Cannot divide by zero";


    private void Start()
    {
        ButtonTapped(CCaption);
        _memoryValue = EmptyCaption;
    }
   
    public void ButtonTapped(string caption)
    {
        if (_divideError)
        {
            ClearCalc();
        }
        bool isNumber = double.TryParse(caption, out double parseResult);
        if (isNumber || caption == CommaCaption)
        {
            if(_numberText.text.Length < 15 || !_hasNumber)
            {
                if (!_hasNumber)
                {
                    _numberText.text = (caption == CommaCaption ? ZeroCaption : EmptyCaption);
                }          
                else if(_numberText.text == ZeroCaption && caption != CommaCaption) 
                {
                    _numberText.text = EmptyCaption;
                }

                if(_numberText.text.Contains(CommaCaption) && caption == CommaCaption)
                {
                    _numberText.text += EmptyCaption;
                }
                else
                {
                    _numberText.text += caption;
                    _hasNumber = true;
                    _canChangeOperator = false;
                }
               
            }
        }
        else if(caption == MSCaption)
        {
            PressedMSButton();
        }
        else if (caption == MRCaption)
        {
            PressedMRButton();
        }
        else if (caption == MCCaption)
        {
            PressedMCButton();
        }
        else if (caption == MPlusCaption)
        {
            PressedMPlusButton();
        }
        else if (caption == MMinusCaption)
        {
            PressedMMinusButton();
        }
        else if (caption == CCaption)
        {
            ClearCalc();
        }
        else if(caption == BackCaption)
        {
            PressedBackButton();
        }
        else if (caption == PlusMinusCaption)
        {
            PressedChangeSignButton();
        }
        else if (caption == PercentCaption)
        {
            PressedPercentButton();
        }
        else if (caption == OneDivideXCaption)
        {
            PressedOneDivideXButton();
        }
        else if (caption == SecondDegreeCaption)
        {
            PressedSecondDegreeButton();
        }
        else if (caption == LogarithmCaption)
        {
            PressedLogButton();
        }
        else if (caption == RootCaption)
        {
            PressedRootButton();
        }
        else if (_hasNumber || _storedOperator == EqualsCaption || _specialAction || _canChangeOperator)
        {           
            if (!_canChangeOperator) 
            {
                _currentValue = double.Parse(_numberText.text);
                _secondPartOfPerformedOperation = _currentValue.ToString();
                _hasNumber = false;
             
                if (_storedOperator != SpaceCaption)
                {
                    CalcResult(_storedOperator);
                    _storedOperator = SpaceCaption;
                }
                _storedValue = _currentValue;
                UpdateDigitLabel();               
                _specialAction = false;
                _canChangeOperator = true;
            }
            if(caption == EqualsCaption)
            {
                _PerformedOperationsText.text = _firstPartOfPerformedOperation + _secondPartOfPerformedOperation;
            }
            else
            {
                _firstPartOfPerformedOperation = _storedValue.ToString() + caption;
                _PerformedOperationsText.text = _firstPartOfPerformedOperation;
            }                        
            _operatorText.text = caption;
            _storedOperator = caption;
        }
    }

    private void ClearCalc()
    {
        _firstPartOfPerformedOperation = _secondPartOfPerformedOperation = _PerformedOperationsText.text = EmptyCaption;      
        _numberText.text = ZeroCaption;
        _operatorText.text = EmptyCaption;
        _specialAction = _hasNumber = _divideError = false;
        _storedOperator = SpaceCaption;
    }

    private void UpdateDigitLabel()
    {
        if (!_divideError)
        {
            _numberText.text = _currentValue.ToString();
        }
        _hasNumber = false;
    }
    private void CalcResult(string activeOperator)
    {
        switch (activeOperator)
        {
            case EqualsCaption:
                _result = _currentValue;
                break;
            case PlusCaption:
                _result = _storedValue + _currentValue;
                break;
            case MinusCaption:
                _result = _storedValue - _currentValue;
                break;
            case DivideCaption:
                if (_currentValue != 0)
                {
                    _result = _storedValue / _currentValue;
                }
                else
                {
                    _divideError = true;
                    _numberText.text = DivideErrorText;
                }
                break;
            case MultiplyCaption:
                _result = _storedValue * _currentValue;
                break;
        }
        _currentValue = _result;
        UpdateDigitLabel();
    }

    private void PressedMSButton()
    {
        if (!_divideError)
        {
            _memoryValue = _numberText.text;
        }
    }
    private void PressedMRButton()
    {
        if (_memoryValue != EmptyCaption)
        {
            _numberText.text = _memoryValue;
        }
        _hasNumber = true;
    }
    private void PressedMCButton()
    {
        _memoryValue = EmptyCaption;
    }
    private void PressedMPlusButton()
    {
        if (!_divideError && _memoryValue != EmptyCaption)
        {
            _memoryValue = Convert.ToString(Double.Parse(_memoryValue) + Double.Parse(_numberText.text));          
        }
    }
    private void PressedMMinusButton()
    {
        if (!_divideError && _memoryValue != EmptyCaption)
        {
            _memoryValue = Convert.ToString(Double.Parse(_memoryValue) - Double.Parse(_numberText.text));
        }
    }
    private void PressedBackButton()
    {
        if (Double.Parse(_numberText.text) == Mathf.Infinity || Double.Parse(_numberText.text) == Mathf.NegativeInfinity)
        {
            _numberText.text = ZeroCaption;
        }
        else if (_numberText.text.Length > 1)
        {
            _numberText.text = _numberText.text.Remove(_numberText.text.Length - 1);
            if (_numberText.text == MinusCaption)
            {
                _numberText.text = ZeroCaption;
            }
        }
        else
        {
            _numberText.text = ZeroCaption;
        }
    }
    private void PressedChangeSignButton()
    {
        _currentValue = -double.Parse(_numberText.text);
        _PerformedOperationsText.text = _currentValue.ToString();
        UpdateDigitLabel();
        _specialAction = true;
    }
    private void PressedPercentButton()
    {
        _currentValue = double.Parse(_numberText.text) / 100;
        _PerformedOperationsText.text = _numberText.text + "%";
        UpdateDigitLabel();
        _specialAction = true;
    }
    private void PressedOneDivideXButton()
    {
        _currentValue = 1 / double.Parse(_numberText.text);
        _PerformedOperationsText.text = "1/(" + _numberText.text + ")";
        UpdateDigitLabel();
        _specialAction = true;
    }
    private void PressedSecondDegreeButton()
    {
        _currentValue = double.Parse(_numberText.text) * double.Parse(_numberText.text);
        _PerformedOperationsText.text = "sqr(" + _numberText.text + ")";
        UpdateDigitLabel();
        _specialAction = true;
    }
    private void PressedLogButton()
    {
        _currentValue = Math.Log(double.Parse(_numberText.text), 10);
        _PerformedOperationsText.text = "log(" + _numberText.text + ")";
        UpdateDigitLabel();
        _specialAction = true;
    }
    private void PressedRootButton()
    {
        _currentValue = Math.Sqrt(double.Parse(_numberText.text));
        _PerformedOperationsText.text = "√(" + _numberText.text + ")";
        UpdateDigitLabel();
        _specialAction = true;
    }

}
