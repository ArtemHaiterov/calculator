using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    [SerializeField] 
    private Button _currentButton;
    [SerializeField] 
    private Text _buttonText;
    [SerializeField] 
    private CalculatorController _calculatorController;

    private void Start()
    {
        _currentButton.onClick.AddListener(ButtonTapped);
    }

    private void OnDestroy()
    {
        _currentButton.onClick.RemoveListener(ButtonTapped);
    }
    private void ButtonTapped()
    {
        _calculatorController.ButtonTapped(_buttonText.text);
    }
}
